# -*- coding: utf-8 -*-

from lxml import etree
from lxml import html
import argparse
import imp
import json
import sys
import unittest
import urllib.request
from multiprocessing import Pool


# Helpers ----------------------------------------------------------------------

def print_error():
    """ Helper method to print exception type and description. """
    print( '!!ERROR!! ', sys.exc_info()[0], sys.exc_info()[1] )


# Testing ----------------------------------------------------------------------

class test_cases( unittest.TestCase ):
    """
    Some test cases.

    - Module dependency test.
    - Property URL generation test.
    - Listing Info extraction test.
    """

    def test_module_requirements( self ):
        """ Test module dependencies. """
        result = True
        required_modules = [
                'argparse',
                'imp',
                'json',
                'lxml',
                'multiprocessing',
                'sys',
                'unittest',
                'urllib', ]

        for m in required_modules:
            try:
                imp.find_module( m )
            except:
                result = False

        self.assertTrue( result, msg='Required modules are available.' )


    def test_get_property_url( self ):
        """ Test property url generation. """
        result = True
        try:
            try:
                get_property_url( -2 )
            except ValueError:
                result = ( result and True )

            try:
                get_property_url( 0 )
            except ValueError:
                result = ( result and True )

            try:
                get_property_url( 'test' )
            except ValueError:
                result = ( result and True )

            try:
                url = get_property_url( 2 )
                self.assertEqual( url, 'https://www.airbnb.co.uk/rooms/2?s=51',
                        msg='Generated property url should match expectation.' )
            except ValueError:
                result = ( result and False )
        except AssertionError:
            raise
        except:
            result = False

        self.assertTrue( result, msg='Property ids should be conformant.' )


    def test_extract_listing_node_from_markup( self ):
        """ Test listing information extraction. """
        markup = None
        with open( 'test_input/test_property.html', 'r' ) as test_input_file:
            markup = test_input_file.read()

        listing = extract_listing_node_from_markup( markup )
        
        listing_expected = None
        with open( 'test_output/listing_node.txt', 'r' ) as test_output_file:
            listing_expected = test_output_file.read()
        
        self.assertEqual( str(listing), str(listing_expected),
                msg='Listing extracting should be functional.' )
    

    def test_listing_node_to_dictionary( self ):
        """ Test listing node to dictionary conversion. """
        markup = None
        with open( 'test_input/test_property.html', 'r' ) as test_input_file:
            markup = test_input_file.read()

        listing_node = extract_listing_node_from_markup( markup )
        listing_dict = listing_node_to_dictionary( 'local', listing_node )
        
        listing_expected = None
        with open( 'test_output/listing_dictionary.txt', 'r' ) as test_output_file:
            listing_expected = test_output_file.read()

        self.assertEqual( str(listing_dict), str(listing_expected),
                msg='Listing dictionaries should be equal.' )


def suite() -> unittest.TestSuite:
    """ Test suite. """
    suite = unittest.TestSuite()
    suite.addTest( test_cases( 'test_module_requirements' ) )
    suite.addTest( test_cases( 'test_get_property_url' ) )
    suite.addTest( test_cases( 'test_extract_listing_node_from_markup' ) )
    suite.addTest( test_cases( 'test_listing_node_to_dictionary' ) )
    return suite


# Scraper ----------------------------------------------------------------------

def get_property_url( property_id: int ) -> str:
    """
    Given a single property id, generates a valid airbnb url.

    :param property_id: integer representing property id.
    :returns str: A string representing the final url.
    :raises: ValueError, if property is of a type other than int.
    :raises: ValueError, if property is <= 0.
    """
    if ( type( property_id ) is not int ):
        raise ValueError( 'Property id should be an int.' )

    if ( property_id <= 0 ):
        raise ValueError( 'A valid property id should be a positive integer.' )

    url_base = 'https://www.airbnb.co.uk/rooms/%s?s=51'
    sid = str(property_id)
    url = url_base % (sid)

    return url


def extract_listing_node_from_markup( markup: str ) -> dict:
    """
    Given html markup as a string, extracts the relevant information node.

    :param markup: String representing the entire page.
    :returns dict: A dictionary containing node information.
    """
    parsed_html = html.fromstring( markup )
    listing_data_script_node = parsed_html.xpath('//script/text()')[4]

    # truncating html comment operators
    json_root = json.loads( listing_data_script_node[4:-3] )
    listing_node = json_root.get('bootstrapData').get('reduxData').get('marketplacePdp').get('listingInfo').get('listing')

    return listing_node


def listing_node_to_dictionary( url: str, listing_node: dict ) -> dict:
    """
    Converts a node to a listing dicitonary with required details.

    :param listing_node: Parent node for all listing information.
    :returns dict: Listing dictionary with required information.
    """

    property_name = str( listing_node.get( 'name' ) )
    property_type = listing_node.get( 'room_and_property_type' )
    number_of_bedrooms = listing_node.get( 'bedroom_label' )
    number_of_bathrooms = listing_node.get( 'bathroom_label' )
    amenities = listing_node.get( 'listing_amenities' )
    filtered_amenities = [ a.get( 'name' ) for a in amenities if a.get( 'is_present' ) ]

    listing_dict = { 'property_url' : url,
            'property_name' : property_name,
            'property_type' : property_type,
            'number_of_bedrooms' : number_of_bedrooms,
            'number_of_bathrooms': number_of_bathrooms,
            'amenities' : filtered_amenities }

    return listing_dict


def scrape_property( property_id: int ) -> dict:
    """
    Given a single property id, scrapes it.

    :param property_id: integer representing property id.
    :returns dict: A dictionary of property information
        property_url, property_name, property_name,
        number_of_bedrooms, number_of_bathrooms, amenities.
        The dictionary may contain all nulls in case of errors.
    :seealso: scrape_properties
    """
    listing_dictionary = None

    try:
        url = get_property_url( property_id )
        # masquerade as a valid user-agent to avoid blocks
        dummy_user_agent = 'Mozilla/5.0 (Windows; U; Linux; en-GB; rv:1.9.0.7) Gecko/2009021910 Firefox/3.0.7'
        rq = urllib.request.Request( url, None, { 'User-Agent':dummy_user_agent } )
        with urllib.request.urlopen( rq ) as rp:
            markup = rp.read()
        
        listing_node = extract_listing_node_from_markup( markup )
        listing_dictionary = listing_node_to_dictionary( url, listing_node )
    except:
        print_error()
        listing_dictionary = { 'property_url' : None,
                'property_name' : None,
                'property_type' : None,
                'number_of_bedrooms' : None,
                'number_of_bathrooms': None,
                'amenities' : None }

    return listing_dictionary


def scrape_properties( property_ids: list, use_multicore: bool ) -> list:
    """
    Given a list of property ids, scrapes them.

    :param property_ids: List of integers representing property id.
    :param use_multicore: Flag to indicate usage of multiple cores.

    :returns list_of_dicts: A list of property information
        dicts as returned by scrape_property.
    :seealso: scrape_property
    """
    if ( use_multicore ):
        pool = Pool()
        return pool.map( scrape_property, property_ids )
    else:
        return  [ scrape_property( pid ) for pid in property_ids ]


# Entry point --------------------------------------------------------------------

def main( use_multicore: bool ) -> int:
    try:
        # default property list.
        ids = [ 14531512 , 19278160, 19292873, 3740403, 6347331 ]
        print( json.dumps( scrape_properties( ids, use_multicore ), sort_keys=False,
            indent=4, ensure_ascii= False ) )
    except:
        print_error()
        return 1

    return 0


def configure_argument_parser() -> argparse.ArgumentParser:
    """
    Create a command line parser as per requirement.

    :returns ArgumentParser: A valid parser.
    """
    parser = argparse.ArgumentParser( description='lairdnd is an airbnb scraper.' )
    parser.add_argument( '-v','--verbose', action='store_true',
            help='Indicate verbosity.' )
    parser.add_argument( '-m','--multicore', action='store_true', default=False,
            help='Use multiple cores.' )
    parser.add_argument( '-t','--test', action='store_true', default=False,
            help='Run tests.' )
    parser.add_argument( '-p','--property_ids', metavar='n,n,n...', type=str,
            help='Specify a comma separated list of valid airbnb property ids.' )
    return parser


if __name__ == '__main__':
    parser = configure_argument_parser()
    args = parser.parse_args()

    use_multicore = args.multicore

    #print( 'use_multicore:', use_multicore )
    print( 'Please standby...' )

    if ( args.test ):
        runner = unittest.TextTestRunner()
        runner.run( suite() )
    elif ( args.property_ids and len( args.property_ids ) > 0 ):
        property_ids = [ int(pid) for pid in args.property_ids.split( ',' ) ]
        print( json.dumps( scrape_properties( property_ids, use_multicore ), sort_keys=False,
            indent=4, ensure_ascii=False ) )
    else:
        sys.exit( main( use_multicore ) )

    sys,exit(0)
